'use strict';

angular.module('NgNotes')

  .controller('NoteDetailsCtrl', function ($rootScope, $scope, $message, Notes, Tags, $sce, $state, $stateParams) {

    $scope.currentNote = Notes.findById($stateParams.id);

    if ($scope.currentNote) {
      $scope.contentHtml = $sce.trustAsHtml($scope.currentNote.html);
    }

    /**
     * Reset inputs for note adding.
     * @private
     */
    function resetForm () {
      $scope.currentNote = {};
    }

    /**
     * Click on new note button
     */
    $scope.newNote = function () {
      $scope.editMode = true;
      $scope.newNoteFlag = true;
      resetForm();
    };

    /**
     * Saving or updating a note.
     */
    $scope.save = function (e) {
      e.preventDefault();
      var data = {
        title: $scope.currentNote.title,
        content: $scope.currentNote.content,
        tags: $scope.currentNote.tags
      };

      Tags.updateTagCloud($scope.currentNote.tags);

      // Saving new note
      if (!$scope.currentNote._id) {
        Notes.save(data).success(function (res) {
          Notes.pushNote(res);
          resetForm();
          $message('Note saved');
        });
        $scope.newNoteFlag = false;
      }
      // Updating notes
      else {
        Notes.update($scope.currentNote._id, data).success(function (res) {
          $scope.currentNote.title = data.title;
          $scope.currentNote.content = data.content;
          $scope.currentNote.html = res.html;
          $scope.contentHtml = $sce.trustAsHtml(res.html);
          $message('Note updated');
        });
      }
    };

    /**
     * Get tags for autocompletion 
     */
    $scope.loadTags = function(query) {
      return Tags.getTagQuery(query);
    };

    $scope.$on('clearNoteForm', function () {
      resetForm();
      $scope.currentNote = {};
      $scope.contentHtml = '';
    });

  });
